
//${WS}/myproject/bundles/HelloWorld/include/HelloWorldActivator.h
#ifndef HELLOWORLDACTIVATOR_H_
#define HELLOWORLDACTIVATOR_H_

#include "celix/dm/DmActivator.h"

class HelloWorldActivator : public celix::dm::DmActivator {
private:
    const std::string word {"C++ World"};
public:
    HelloWorldActivator(celix::dm::DependencyManager& mng) : DmActivator {mng} {}
    virtual void init();
    virtual void deinit();
};

#endif //HELLOWORLDACTIVATOR_H_

